from __future__ import division

def format_expression(s):
    for c in s:
        if c in "+-*/":
            s = s.replace(c, " "+c+" ")
    s = " ".join(s.split())
    return s

def main():
	try:
		exp = raw_input ("Enter expression to calc: ")
		exp = format_expression(exp)
		print "%s = %s" %(exp, eval(exp))
	except:
		print "Invalid input!"

if __name__ == "__main__":
	main()
